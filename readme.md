# Practica con NPM

# Grupo 7CC2  Web Platforms

# Hecho por: Jesus Manuel Calleros Vázquez

# Descripción
### Pequeña aplicación realizada con node donde se ponen a prueba algunos modulos como el de mocha y eslint para testing, aparte del log4js para los logs.


# Requisitos
### Tener node instalado

# Herramientas utilizadas
### Node
### Eslint
### MochaJs
### Log4js

# Lanzamiento de la aplicación

### En la terminal ubicarse en la carpeta de package_manager y lanzar el comando npm start
