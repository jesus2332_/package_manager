const sumar = require('../index');
const assert = require('assert')

describe(' Probar la suma de dos numeros', ()=> {
    //afirmar que cinco mas cinco es 10
    it('Cinco mas cinco es 10', ()=> {
        assert.equal(10, sumar(5,5));
    });
    //Afirmamos que cinco mas cinco no son 55
    it('Cinco mas cinco no es 55',()=>{
        assert.notEqual(55, sumar(5,5));
    })
    
});
//cobertura cantidad de lineas de codigo que las pruebas alcanzan a tocar